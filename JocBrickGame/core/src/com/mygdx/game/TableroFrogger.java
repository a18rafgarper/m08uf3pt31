package com.mygdx.game;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class TableroFrogger {

	Texture tableroFrogger;

	int[] posicionTableroX;
	int[] posicionTableroY;

	Rana rana;
	List<Rectangle> ranasAcabadas;
	List<LineaDeObstaculos> lineasDeObstaculos;

	public TableroFrogger() {
		////////////////////////////////////////////////////////////////////////
		/////////////////////////// CARGAR TABLERO /////////////////////////////
		tableroFrogger = new Texture(Gdx.files.internal("imagenes/fondofrogger.png"));
		posicionTableroX = new int[10];
		posicionTableroX[0] = 553;
		for (int i = 1; i < 10; i++) {
			posicionTableroX[i] = posicionTableroX[i - 1] + 29;
		}
		posicionTableroY = new int[8];
		posicionTableroY[0] = 149;
		for (int i = 1; i < 8; i++) {
			posicionTableroY[i] = posicionTableroY[i - 1] + 56;
		}
		////////////////////////////////////////////////////////////////////////
		/////////////////////////// CARGAR RANA ////////////////////////////////
		rana = new Rana(posicionTableroX, posicionTableroY, 0, 0);
		ranasAcabadas = new ArrayList<Rectangle>();

		////////////////////////////////////////////////////////////////////////
		/////////////////// CARGAR LINIA DE OBSTACULOS /////////////////////////
		lineasDeObstaculos = new ArrayList<LineaDeObstaculos>();
		lineasDeObstaculos.add(new LineaDeObstaculos(1, posicionTableroX, posicionTableroY));
		lineasDeObstaculos.add(new LineaDeObstaculos(2, posicionTableroX, posicionTableroY));
		lineasDeObstaculos.add(new LineaDeObstaculos(3, posicionTableroX, posicionTableroY));
		lineasDeObstaculos.add(new LineaDeObstaculos(4, posicionTableroX, posicionTableroY));
		lineasDeObstaculos.add(new LineaDeObstaculos(5, posicionTableroX, posicionTableroY));
		lineasDeObstaculos.add(new LineaDeObstaculos(6, posicionTableroX, posicionTableroY));

	}

	public void jugandoTablero(SpriteBatch batch) {
		dibujarTablero(batch);
		movimentosRana();
		ranaChoca();
	}

	public void dibujarTablero(SpriteBatch batch) {
		batch.draw(tableroFrogger, 555, 150);
		rana.dibujarRana(batch, posicionTableroX, posicionTableroY);
		dibujarRanasAcabadas(batch);

		for (int i = 0; i < lineasDeObstaculos.size(); i++) {
			lineasDeObstaculos.get(i).dibujarLiniaDeObstaculo(batch, posicionTableroX, posicionTableroY);
		}

	}

	public void dibujarRanasAcabadas(SpriteBatch batch) {
		for (Rectangle froggerAcabat : ranasAcabadas) {
			batch.draw(rana.froggerCuadradoNegroTexture, froggerAcabat.x, froggerAcabat.y, froggerAcabat.width,
					froggerAcabat.height);
		}
		if (ranasAcabadas.size() == 10) {
			reiniciarTablero();
			PantallaFrogger.incrementarPuntaucionTableroAcabado();
			LineaDeObstaculos.incrementarVelocidad();
		}
	}

	public void reiniciarTablero() {
		ranasAcabadas.removeAll(ranasAcabadas);
	}

	public void movimentosRana() {
		rana.parpadeado();
		rana.moverRana(posicionTableroX, posicionTableroY, ranasAcabadas);
	}

	public void ranaChoca() {
		if (rana.chocarConRanasAcabadas(ranasAcabadas)) {
			rana.ranaVuelveAEmpezar(posicionTableroX, posicionTableroY);
		}

		for (int i = 0; i < lineasDeObstaculos.size(); i++) {
			if (rana.chocarConObstaculo(lineasDeObstaculos.get(i).linia, lineasDeObstaculos.get(i).posicionObstaculo,
					posicionTableroX, posicionTableroY)) {
				rana.ranaVuelveAEmpezar(posicionTableroX, posicionTableroY);
			}
		}
	}

}

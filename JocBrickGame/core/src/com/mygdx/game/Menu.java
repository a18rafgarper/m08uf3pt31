package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class Menu extends ScreenAdapter {

	BrinkGame brickGame;
	Sound soundPuntuacion;

	public Menu(BrinkGame frogger) {
		this.brickGame = frogger;
		soundPuntuacion = Gdx.audio.newSound(Gdx.files.getFileHandle("sounds/soundseleccion.mp3", FileType.Internal));
	}

	@Override
	public void render(float delta) {

		BitmapFont fontGameOver = new BitmapFont();
		brickGame.batch.begin();
		fontGameOver.setColor(Color.BLACK);
		fontGameOver.getData().setScale(5, 5);
		fontGameOver.draw(brickGame.batch, "MENU", 530, 700);
		fontGameOver.getData().setScale(5, 5);
		fontGameOver.draw(brickGame.batch, "1. Frogger", 500, 450);
		fontGameOver.getData().setScale(5, 5);
		fontGameOver.draw(brickGame.batch, "2. Salir", 500, 350);
		brickGame.batch.end();

		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_1)) {
			brickGame.setScreen(new PantallaFrogger(brickGame));
			soundPuntuacion.play(1f);

		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2)) {
			soundPuntuacion.play(1f);
			System.exit(0);
		}

	}

}

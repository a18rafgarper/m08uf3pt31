package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class PantallaFrogger extends ScreenAdapter {

	BrinkGame brickGame;
	TableroFrogger tableroFrogger;
	static int puntuacion;
	static Sound soundPuntuacion;
	
	public PantallaFrogger(BrinkGame brickGame) {
		this.brickGame = brickGame;
		inicializarPuntaucion();
		tableroFrogger = new TableroFrogger();
	
		soundPuntuacion = Gdx.audio.newSound(Gdx.files.getFileHandle("sounds/soundpuntuacion.mp3", FileType.Internal));

	}

	public static void inicializarPuntaucion() {
		puntuacion = 0;
	}
	
	public static void incrementarPuntaucionRanaAcabada() {
		puntuacion = puntuacion + 100;
		soundPuntuacion.play(1f);
	}

	public static void incrementarPuntaucionTableroAcabado() {
		puntuacion = puntuacion + 1000;
		soundPuntuacion.play(1f);
	}

	public void jugarPantalla(SpriteBatch batch) {
		
		tableroFrogger.jugandoTablero(batch);
		dibujarPuntuacion(batch);
		dibujarVidas(batch);
		if (tableroFrogger.rana.vida == 0) {
			llamarGameOver();
		}
	}
	
	public void llamarGameOver () {
		brickGame.setScreen(new GameOverFrogger(brickGame));

	}
	
	@Override
	public void render(float delta) {
		brickGame.batch.begin();
		jugarPantalla(brickGame.batch);	
		brickGame.batch.end();

	}

	public void dibujarVidas(SpriteBatch batch) {
		BitmapFont fontGameOver = new BitmapFont();
		fontGameOver.setColor(new Color().BLACK);
		fontGameOver.getData().setScale(2, 2);
		fontGameOver.draw(batch, "Vida: \n\n"+ Rana.vida, 980, 500);

	}
	
	public void dibujarPuntuacion(SpriteBatch batch) {
		BitmapFont fontGameOver = new BitmapFont();
		fontGameOver.setColor(new Color().BLACK);
		fontGameOver.getData().setScale(2, 2);
		fontGameOver.draw(batch, "Puntuacion:\n\n"+ puntuacion, 980, 700);
	}

}

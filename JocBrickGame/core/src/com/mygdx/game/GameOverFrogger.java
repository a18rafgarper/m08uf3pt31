package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class GameOverFrogger extends ScreenAdapter {

	BrinkGame brickGame;
	TextButton buttonReintentar;

	public GameOverFrogger(BrinkGame brickGame) {
		this.brickGame = brickGame;
	}

	@Override
	public void render(float delta) {
		BitmapFont fontGameOver = new BitmapFont();
		brickGame.batch.begin();
		fontGameOver.setColor(Color.BLACK);
		fontGameOver.getData().setScale(5, 5);
		fontGameOver.draw(brickGame.batch, "GAME OVER", 500, 700);
		fontGameOver.getData().setScale(5, 5);
		fontGameOver.draw(brickGame.batch, "Puntuacion: " + PantallaFrogger.puntuacion, 500, 550);
		fontGameOver.getData().setScale(5, 5);
		fontGameOver.draw(brickGame.batch, "1. Reintentar", 500, 350);
		fontGameOver.getData().setScale(5, 5);
		fontGameOver.draw(brickGame.batch, "2. Salir Al Menu", 500, 250);
		brickGame.batch.end();

		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_1)) {
			brickGame.setScreen(new PantallaFrogger(brickGame));
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2)) {
			brickGame.setScreen(new Menu(brickGame));
		}
	}
}
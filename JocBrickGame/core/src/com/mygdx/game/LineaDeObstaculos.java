package com.mygdx.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class LineaDeObstaculos {

	Texture cuadradoNegroTexture;
	List<Rectangle> lineaDeObstaculos;
	int[] posicionObstaculo;
	long tiempoOriginal;
	static int velocidad = 1000;
	int linia;

	public LineaDeObstaculos(int linia, int[] posicionTableroX, int[] posicionTableroY) {
		this.linia = linia;
		cuadradoNegroTexture = new Texture(Gdx.files.internal("imagenes/cuadradonegro.png"));
		elegirFormatoDeLaLinea(linia, posicionTableroX, posicionTableroY);
		tiempoOriginal = System.currentTimeMillis();
	}

	public static void incrementarVelocidad () {
		velocidad = velocidad - 20;
	}
	
	
	private void elegirFormatoDeLaLinea(int linia, int[] posicionTableroX, int[] posicionTableroY) {
		lineaDeObstaculos = new ArrayList<Rectangle>();
		if (linia == 1 || linia == 2 || linia == 3) {
			crearObstaculoFormato1(posicionTableroX, posicionTableroY);
		} else if (linia == 4 || linia == 5 || linia == 6) {
			crearObstaculoFormato2(posicionTableroX, posicionTableroY);
		}

	}

	private void crearObstaculoFormato1(int[] posicionTableroX, int[] posicionTableroY) {
		posicionObstaculo = formato1DeLinia();

		lineaDeObstaculos.add(new Rectangle(posicionTableroX[posicionObstaculo[0]], posicionTableroY[linia], 28, 27));
		lineaDeObstaculos.add(new Rectangle(posicionTableroX[posicionObstaculo[1]], posicionTableroY[linia], 28, 27));
		lineaDeObstaculos.add(new Rectangle(posicionTableroX[posicionObstaculo[2]], posicionTableroY[linia], 28, 27));
		lineaDeObstaculos.add(new Rectangle(posicionTableroX[posicionObstaculo[3]], posicionTableroY[linia], 28, 27));
		lineaDeObstaculos.add(new Rectangle(posicionTableroX[posicionObstaculo[4]], posicionTableroY[linia], 28, 27));
	}

	private void crearObstaculoFormato2(int[] posicionTableroX, int[] posicionTableroY) {
		posicionObstaculo = formato2DeLinia();

		lineaDeObstaculos.add(new Rectangle(posicionTableroX[posicionObstaculo[0]], posicionTableroY[linia], 28, 27));
		lineaDeObstaculos.add(new Rectangle(posicionTableroX[posicionObstaculo[1]], posicionTableroY[linia], 28, 27));
		lineaDeObstaculos.add(new Rectangle(posicionTableroX[posicionObstaculo[2]], posicionTableroY[linia], 28, 27));
		lineaDeObstaculos.add(new Rectangle(posicionTableroX[posicionObstaculo[3]], posicionTableroY[linia], 28, 27));
		lineaDeObstaculos.add(new Rectangle(posicionTableroX[posicionObstaculo[4]], posicionTableroY[linia], 28, 27));
		lineaDeObstaculos.add(new Rectangle(posicionTableroX[posicionObstaculo[5]], posicionTableroY[linia], 28, 27));
	}

	// 2 y 4 espacios 2 y 2 bloques
	public int[] formato2DeLinia() {
		int[] valorPosicionObstaculo = new int[6];
		valorPosicionObstaculo[0] = (int) (Math.random() * 10 + 0);
		valorPosicionObstaculo[1] = valorPosicionObstaculo[0] + 1;
		if (valorPosicionObstaculo[1] >= 10) {
			valorPosicionObstaculo[1] = 0;
		}
		valorPosicionObstaculo[2] = valorPosicionObstaculo[1] + 3;
		if (valorPosicionObstaculo[2] >= 10) {
			if (valorPosicionObstaculo[2] == 10) {
				valorPosicionObstaculo[2] = 0;
			} else if (valorPosicionObstaculo[2] == 11) {
				valorPosicionObstaculo[2] = 1;
			} else if (valorPosicionObstaculo[2] == 12) {
				valorPosicionObstaculo[2] = 2;
			}
		}		
		valorPosicionObstaculo[3] = valorPosicionObstaculo[2] + 1;
		if (valorPosicionObstaculo[3] >= 10) {
			valorPosicionObstaculo[3] = 0;
		}
		valorPosicionObstaculo[4] = valorPosicionObstaculo[3] + 1;
		if (valorPosicionObstaculo[4] >= 10) {
			valorPosicionObstaculo[4] = 0;
		}
		valorPosicionObstaculo[5] = valorPosicionObstaculo[4] + 1;
		if (valorPosicionObstaculo[5] >= 10) {
			valorPosicionObstaculo[5] = 0;
		}
		
		return valorPosicionObstaculo;
	}
	// 2 y 3 espacios 3 y 2 bloques
	public int[] formato1DeLinia() {
		int[] valorPosicionObstaculo = new int[5];
		valorPosicionObstaculo[0] = (int) (Math.random() * 10 + 0);
		valorPosicionObstaculo[1] = valorPosicionObstaculo[0] + 1;
		if (valorPosicionObstaculo[1] >= 10) {
			valorPosicionObstaculo[1] = 0;
		}
		valorPosicionObstaculo[2] = valorPosicionObstaculo[1] + 3;
		if (valorPosicionObstaculo[2] >= 10) {
			if (valorPosicionObstaculo[2] == 10) {
				valorPosicionObstaculo[2] = 1;
			} else if (valorPosicionObstaculo[2] == 11) {
				valorPosicionObstaculo[2] = 2;
			} else if (valorPosicionObstaculo[2] == 12) {
				valorPosicionObstaculo[2] = 3;
			}
		}

		valorPosicionObstaculo[3] = valorPosicionObstaculo[2] + 1;
		if (valorPosicionObstaculo[3] >= 10) {
			valorPosicionObstaculo[3] = 0;
		}
		valorPosicionObstaculo[4] = valorPosicionObstaculo[3] + 1;
		if (valorPosicionObstaculo[4] >= 10) {
			valorPosicionObstaculo[4] = 0;
		}
		return valorPosicionObstaculo;
	}

	public void dibujarLiniaDeObstaculo(SpriteBatch batch, int[] posicionTableroX, int[] posicionTableroY) {
		if (linia % 2 == 0) {
			moverDerecha(posicionTableroX, posicionTableroY);
			for (Rectangle obstaculo : lineaDeObstaculos) {
				batch.draw(cuadradoNegroTexture, obstaculo.x, obstaculo.y, obstaculo.width, obstaculo.height);
			}
		} else {
			moverIzquierda(posicionTableroX, posicionTableroY);
			for (Rectangle obstaculo : lineaDeObstaculos) {
				batch.draw(cuadradoNegroTexture, obstaculo.x, obstaculo.y, obstaculo.width, obstaculo.height);
			}
		}
	}

	public void moverDerecha(int[] posicionTableroX, int[] posicionTableroY) {
		long segundoPasadoFinal = System.currentTimeMillis();

		if ((segundoPasadoFinal - tiempoOriginal) > velocidad) {
//Se restara la posicion del Obstaculo.
			for (int i = 0; i < posicionObstaculo.length; i++) {
				posicionObstaculo[i] = posicionObstaculo[i] - 1;
			}
//Se comprobara si sale del Array, si es asi empezara de por la derecha.
			for (int i = 0; i < posicionObstaculo.length; i++) {
				if (posicionObstaculo[i] == -1) {
					posicionObstaculo[i] = 9;
				}
			}
//Se actualizara la el Obstaculo 
			for (int i = 0; i < posicionObstaculo.length; i++) {
				lineaDeObstaculos.set(i,
						new Rectangle(posicionTableroX[posicionObstaculo[i]], posicionTableroY[linia], 28, 27));
			}
			tiempoOriginal = System.currentTimeMillis();
		}
	}

	public void moverIzquierda(int[] posicionTableroX, int[] posicionTableroY) {
		long segundoPasadoFinal = System.currentTimeMillis();

		if ((segundoPasadoFinal - tiempoOriginal) > velocidad) {
//Se sumara la posicion del Obstaculo.
			for (int i = 0; i < posicionObstaculo.length; i++) {
				posicionObstaculo[i] = posicionObstaculo[i] + 1;
			}
//Se comprobara si sale del Array, si es asi empezara de por la derecha.
			for (int i = 0; i < posicionObstaculo.length; i++) {
				if (posicionObstaculo[i] == 10) {
					posicionObstaculo[i] = 0;
				}
			}
//Se actualizara la el Obstaculo 
			for (int i = 0; i < posicionObstaculo.length; i++) {
				lineaDeObstaculos.set(i,
						new Rectangle(posicionTableroX[posicionObstaculo[i]], posicionTableroY[linia], 28, 27));
			}
			tiempoOriginal = System.currentTimeMillis();
		}
	}

}

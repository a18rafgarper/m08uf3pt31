package com.mygdx.game;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Rana {

	Rectangle frogger;
	Texture froggerCuadradoNegroTexture;
	Texture froggerCuadradoBlancoTexture;

	boolean parpadeo;
	int posicionFroggerX;
	int posicionFroggerY;

	long tiempoParpadeoOriginal;
	static int vida;

	Sound soundMovimiento;
	Sound soundChocar;

	public Rana(int[] posicionTableroX, int[] posicionTableroY, int posicionFroggerX, int posicionFroggerY) {
		this.posicionFroggerX = posicionFroggerX;
		this.posicionFroggerY = posicionFroggerY;

		frogger = new Rectangle(posicionTableroX[posicionFroggerX], posicionTableroY[posicionFroggerY], 28, 27);

		froggerCuadradoNegroTexture = new Texture(Gdx.files.internal("imagenes/cuadradonegro.png"));
		froggerCuadradoBlancoTexture = new Texture(Gdx.files.internal("imagenes/cuadradoblanco.png"));

		parpadeo = false;
		tiempoParpadeoOriginal = System.currentTimeMillis();

		vida = 3;

		soundMovimiento = Gdx.audio.newSound(Gdx.files.getFileHandle("sounds/soundmovimiento.wav", FileType.Internal));
		soundChocar = Gdx.audio.newSound(Gdx.files.getFileHandle("sounds/soundchocar.wav", FileType.Internal));
	}

	public boolean chocarConObstaculo(int linia, int[] posicionObstaculo, int[] posicionTableroX,
			int[] posicionTableroY) {
		for (int i = 0; i < posicionObstaculo.length; i++) {
			if (posicionTableroX[posicionObstaculo[i]] == frogger.x && posicionTableroY[linia] == frogger.y) {
				vida--;
				soundChocar.play(1f);
				return true;
			}
		}
		return false;
	}

	public boolean chocarConRanasAcabadas(List<Rectangle> ranasAcabadas) {
		for (int i = 0; i < ranasAcabadas.size() - 1; i++) {
			if (ranasAcabadas.get(i).x == ranasAcabadas.get(ranasAcabadas.size() - 1).x
					&& ranasAcabadas.get(i).y == ranasAcabadas.get(ranasAcabadas.size() - 1).y) {
				ranasAcabadas.remove(ranasAcabadas.size() - 1);
				vida--;
				soundChocar.play(1f);
				return true;
			}
		}
		return false;
	}

	public void dibujarRana(SpriteBatch batch, int[] posicionTableroX, int[] posicionTableroY) {
		if (!parpadeo) {
			batch.draw(froggerCuadradoNegroTexture, posicionTableroX[posicionFroggerX], frogger.y, frogger.width,
					frogger.height);
		} else {
			batch.draw(froggerCuadradoBlancoTexture, posicionTableroX[posicionFroggerX], frogger.y, frogger.width,
					frogger.height);
		}
	}

	public void parpadeado() {
		long tiempoParpadeoFinal = System.currentTimeMillis();
		if ((tiempoParpadeoFinal - tiempoParpadeoOriginal) > 200) {
			if (!parpadeo) {
				// Seva la imagen
				parpadeo = true;
			} else {
				// Vuelve la imagen
				parpadeo = false;
			}
			tiempoParpadeoOriginal = System.currentTimeMillis();
		}
	}

	public void moverRana(int[] posicionTableroX, int[] posicionTableroY, List<Rectangle> ranasAcabadas) {

		if (Gdx.input.isKeyJustPressed(Keys.LEFT) || Gdx.input.isKeyJustPressed(Keys.A)) {
			// Anem a la esquerra.
			if (posicionFroggerX == 0) {
				posicionFroggerX = 9;
			} else {
				posicionFroggerX--;
				frogger.x = posicionTableroX[posicionFroggerX];
			}
			soundMovimiento.play(1f);

		}
		if (Gdx.input.isKeyJustPressed(Keys.RIGHT) || Gdx.input.isKeyJustPressed(Keys.D)) {
			// Anem a la dreta.
			if (posicionFroggerX == 9) {
				posicionFroggerX = 0;
			} else {
				posicionFroggerX++;
				frogger.x = posicionTableroX[posicionFroggerX];
			}
			soundMovimiento.play(1f);

		}
		if (Gdx.input.isKeyJustPressed(Keys.UP) || Gdx.input.isKeyJustPressed(Keys.W)) {
			// Anem a la adalt.
			if (posicionFroggerY == 6) {

				Rectangle newfrogger = new Rectangle(posicionTableroX[posicionFroggerX],
						posicionTableroY[posicionFroggerY + 1], 28, 27);
				ranasAcabadas.add(newfrogger);
				if (!chocarConRanasAcabadas(ranasAcabadas)) {
					PantallaFrogger.incrementarPuntaucionRanaAcabada();
				}
				ranaVuelveAEmpezar(posicionTableroX, posicionTableroY);

			} else {
				posicionFroggerY++;
				frogger.y = posicionTableroY[posicionFroggerY];
			}
			soundMovimiento.play(1f);
		}
		if (Gdx.input.isKeyJustPressed(Keys.DOWN) || Gdx.input.isKeyJustPressed(Keys.S)) {
			// Anem a la adalt.
			if (posicionFroggerY == 0) {
				posicionFroggerY = 0;
			} else {
				posicionFroggerY--;
				frogger.y = posicionTableroY[posicionFroggerY];
			}
			soundMovimiento.play(1f);
		}
	}

	public void ranaVuelveAEmpezar(int[] posicionTableroX, int[] posicionTableroY) {
		posicionFroggerY = 0;
		frogger.y = posicionTableroY[posicionFroggerY];
		posicionFroggerX = 0;
		frogger.x = posicionTableroX[posicionFroggerX];
	}
}
